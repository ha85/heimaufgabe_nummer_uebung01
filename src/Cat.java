import javax.swing.*;

/**
 * Diese Klasse repräsentiert eine Katze.
 * Der Autor dieser Klasse ist [Dein Name].
 * Version 1.0
 */
public class Cat {

    /**
     * Diese Methode ist der Einstiegspunkt des Programms.
     * Sie fordert den Benutzer auf, den Namen und das Alter der Katze einzugeben.
     * Dann erstellt sie eine neue Katze mit den eingegebenen Daten und zeigt Informationen über die Katze an.
     *
     * @param args Die Befehlszeilenargumente (nicht verwendet).
     */
    public static void main(String[] args) {
        // Benutzer zur Eingabe des Namens der Katze auffordern
        String name = JOptionPane.showInputDialog("Geben Sie den Namen der Katze ein:");

        // Benutzer zur Eingabe des Alters der Katze auffordern
        String ageStr = JOptionPane.showInputDialog("Geben Sie das Alter der Katze ein:");

        // String in Integer umwandeln
        int age = Integer.parseInt(ageStr);

        // Eine neue Katze mit den eingegebenen Daten erstellen
        Cat myCat = new Cat(name, age);

        // Informationen über die Katze anzeigen
        JOptionPane.showMessageDialog(null, "Meine Katze heißt " + myCat.getName() + " und ist " + myCat.getAge() + " Jahre alt.");
    }

    private String name;
    private int age;

    /**
     * Konstruktor für die Klasse Cat.
     *
     * @param name Der Name der Katze.
     * @param age  Das Alter der Katze.
     */
    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * Gibt den Namen der Katze zurück.
     *
     * @return Der Name der Katze.
     */
    public String getName() {
        return name;
    }

    /**
     * Gibt das Alter der Katze zurück.
     *
     * @return Das Alter der Katze.
     */
    public int getAge() {
        return age;
    }
}
